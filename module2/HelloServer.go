package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
)

func main() {
	http.HandleFunc("/", HelloServer)
	http.ListenAndServe("localhost:8888",nil)
}

func HelloServer(w http.ResponseWriter, r *http.Request) {
	var retStatusCode = 200
	if strings.Compare(r.URL.Path, "/healthz") == 0 {
		w.WriteHeader(200)
		retStatusCode = 200
		log.Printf("Response Status Code: %d\n", http.StatusOK)
	} else {
		w.WriteHeader(404)
		retStatusCode=404
		log.Printf("Response Status Code: %d\n", http.StatusNotFound)
	}
	fmt.Fprintf(w, "Status Code: %d\n", retStatusCode)

	fmt.Fprintf(w, "Request Headers: %d\n", r.Header)

	fmt.Fprintf(w, "OS env variable PROCESSOR_IDENTIFIER:  %s\n", os.Getenv("PROCESSOR_IDENTIFIER"))

	log.Printf("Remote Host: %s", r.Host)

}
